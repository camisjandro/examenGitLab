 Command line instructions
Git global setup

git config --global user.name "Jandro Camisetas"
git config --global user.email "camisjandro@gmail.com"

Create a new repository

git clone https://camisjandro@gitlab.com/camisjandro/examenGitLab.git
cd examenGitLab
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin https://camisjandro@gitlab.com/camisjandro/examenGitLab.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin https://camisjandro@gitlab.com/camisjandro/examenGitLab.git
git push -u origin --all
git push -u origin --tags

